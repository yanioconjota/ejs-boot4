# Template Bootstrap 4

![](https://i.imgur.com/W0yMxON.png)


>Template de Bootstrap básico para trabajar con Javascript y jQuery.

##### Para utlizar este template debes tener instalado node y sass:
- Primero instalar [Node.js](https://nodejs.org/) v4+
- Luego Sass ejecutando el siguiente comando desde la consola:  
```sh
$ npm install -g sass
```

##### Instalar las respectivas dependencias con:  
```sh
$ cd <ruta del template>
$ npm install
```

##### Ejecutar sass:  
```sh
$ sass --watch public/scss:public/css
```

*Realizado por [Janio Isacura](http://janioisacura.com/)*
